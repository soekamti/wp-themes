<header class="main-header">
  <div class="">
    <div class="navigation">
      <a class="brand" href="<?= esc_url(home_url('/')); ?>"><img src="<?= get_template_directory_uri(); ?>/euforia-store-logo.png" alt=""></a>
      <nav class="nav-primary">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif;
        ?>
      </nav>
    </div>
    <div class="search-box">
      
        search box here
      
    </div>
  </div>
</header>