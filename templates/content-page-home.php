<div class="home">
	<div class="banner">
		<img src="<?php echo get_template_directory_uri() ?>/assets/images/banner promo 1.jpg" alt="">
	</div>
	<div class="greeting">
		<h2>Hello! Selamat datang dan selamat berbelanja!</h2>
		<h3>Belanja kamera dan asesorisnya dengan mudah dan lengkap</h3>
	</div>
	<div class="featured-category">
		<ul class="list-featured-category">
			<li>
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/featured-cam.png" alt="">
				<h3>Camera & Lenses</h3>
				<p>Pilihan berbagai kamera dari DSLR,Mirrorless, Pocket, Action Cam hingga Kamera instan dan berbagai lensa ada disini semua.</p>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/featured-tripod.png" alt="">
				<h3>Supporting Tools</h3>
				<p>Mencari Tripod, Slider, Stabilizer hingga Portable Jib? ada disini!</p>
			</li>
			<li>
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/featured-cable.png" alt="">
				<h3>Accesories</h3>
				<p>Kebutuhan asesoris untuk gadget kamu mulai dari Kabel Data, Memory Card, Adapter, dan berbagai asesoris pendukung lain tersedia di sini.</p>
			</li>
		</ul>
	</div>
	<div class="hot-items">
		<h2>HOT ITEMS</h2>
		<h4>Berbagai pilihan barang terpopuler di Euforia Store ada disini</h4>
		<ul class="list-hot-items">
			<li></li>
		</ul>
	</div>
	<div class="special-promo">
		<h2>SPECIAL PROMO</h2>
		<h3>Berbagai promo spesial yang rugi jika anda lewatkan!</h3>
		<ul class="list-special-promo">
			<li></li>
		</ul>
	</div>
	<div class="second-greeting">
		logo euforiastore
	</div>
</div>